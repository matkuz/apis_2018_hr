package interfaces;

import org.opencv.core.Mat;


public interface ImageStreamSource {
    Mat getImageSource();
}
