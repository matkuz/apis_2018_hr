package interfaces;

import listeners.DrawTechniqueListener;
import listeners.EditStateChangeListener;

public interface EditPanelButtonsInterface extends DrawTechniqueListener, EditStateChangeListener {
}
