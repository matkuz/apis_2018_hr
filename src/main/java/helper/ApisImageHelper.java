package helper;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import video.Mat2Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ApisImageHelper {

    public static final int RED_MIN = 60;
    public static final int RED_MAX = 125;

    public static final int GREEN_MIN = 40;
    public static final int GREEN_MAX = 110;

    public static final int BLUE_MIN = 40;
    public static final int BLUE_MAX = 110;

    public static BufferedImage convertMat2BufferedImage(Mat mat) {
        return new Mat2Image().getImage(mat);
    }

    public static Mat convertBufferedImage2Mat(BufferedImage image) {
        Mat resMat = new Mat();
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", byteArrayOutputStream);
            byteArrayOutputStream.flush();
            resMat = Imgcodecs.imdecode(new MatOfByte(byteArrayOutputStream.toByteArray()), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
        } catch (IOException e) {
            System.out.println("Blad podczas konwersji BufferedImage to Mat");
        }
        return resMat;
    }

    public static BufferedImage getSkinColorBin(Mat matFromCamera) {
        BufferedImage bufferedImage = ApisImageHelper.convertMat2BufferedImage(matFromCamera);
        Integer width = bufferedImage.getWidth();
        Integer height = bufferedImage.getHeight();
        BufferedImage bi = new BufferedImage(width, height, bufferedImage.getType());
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int rgb = bufferedImage.getRGB(i, j);

                int r = (rgb >> 16) & 0xff;
                int g = (rgb >> 8) & 0xff;
                int b = (rgb >> 0) & 0xff;

                if ((r > RED_MIN && r < RED_MAX) && (g > GREEN_MIN && g < GREEN_MAX) && (b > BLUE_MIN && b < BLUE_MAX)) {
                    r = 255;
                    g = 255;
                    b = 255;
                } else {
                    r = 0;
                    g = 0;
                    b = 0;
                }


                rgb = (rgb & 0xff000000) | (r << 16) | (g << 8) | (b << 0);
                bi.setRGB(i, j, rgb);
            }
        }
        return bi;
    }

    public static Mat getCleanBlackImage(Mat matFromCamera) {
        BufferedImage bufferedImage = ApisImageHelper.convertMat2BufferedImage(matFromCamera);
        Integer width = bufferedImage.getWidth();
        Integer height = bufferedImage.getHeight();
        BufferedImage bi = new BufferedImage(width, height, bufferedImage.getType());
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                bi.setRGB(i, j, 0);
            }
        }
        return convertBufferedImage2Mat(bi);
    }

    public static Optional<MatOfPoint> getBiggestContour(Mat outMat) {
        Mat dstMat = new Mat();
        Mat srcMat = outMat.clone();
        Imgproc.cvtColor(srcMat, dstMat, Imgproc.COLOR_RGB2GRAY);
        List<MatOfPoint> contours = new ArrayList<>();

        Imgproc.findContours(dstMat, contours, dstMat, Imgproc.RETR_LIST,
                Imgproc.CHAIN_APPROX_SIMPLE);

        //sortowanie po rozmiarze konturu
        return contours.stream().max(Comparator.comparing(Imgproc::contourArea));
    }

    public static Mat getMorphImage(Mat inputImage) {
        Mat outputImage = new Mat();

        //wykrycie dloni
        BufferedImage skinBufferedImage = ApisImageHelper.getSkinColorBin(inputImage);

        //zamiana na Mat
        Mat matSkin = ApisImageHelper.convertBufferedImage2Mat(skinBufferedImage);

        //przeksztalcenia morfologiczne
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(18, 18));
        Imgproc.morphologyEx(matSkin, outputImage, Imgproc.MORPH_OPEN, kernel);

        return outputImage;
    }
}


