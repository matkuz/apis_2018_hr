package gui;

import interfaces.EditPanelButtonsInterface;
import listeners.EditStateChangeListener;
import listeners.DrawTechniqueListener;
import listeners.PictureActionListener;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;

public class EditButtonsPanel extends JPanel {
    private final PictureActionListener pictureActionListener;
    private final EditPanelButtonsInterface editPanelButtonsInterface;
    private JButton nextImageButton;
    private JButton previousImageButton;
    private JButton editButton;
    private JButton loadButton;
    private JButton loadSeriesButton;
    private JButton drawByHandButton;
    private boolean editMode = true;
    private boolean mouseMode = true;

    EditButtonsPanel(PictureActionListener pictureActionListener, EditPanelButtonsInterface editPanelButtonsInterface) {
        super();
        this.pictureActionListener = pictureActionListener;
        this.editPanelButtonsInterface = editPanelButtonsInterface;
        initGUI();
        initBehaviour();
        setEditButtonEnabled(false);
    }

    private void initBehaviour() {
        loadButton.addActionListener(e -> fireLoadAction());
        loadSeriesButton.addActionListener(e -> fireLoadSeriesAction());
        editButton.addActionListener(e -> {
            if (!editMode) {
                fireEditMode();
            } else {
                fireRubberMode();
            }
        });
        drawByHandButton.addActionListener(e -> {
            if (!mouseMode) {
                fireHandMode();
            } else {
                fireMouseMode();
            }
        });
        nextImageButton.addActionListener( e -> fireNextImage());
        previousImageButton.addActionListener(e -> firePrevImage());
    }

    private void firePrevImage() {
        pictureActionListener.previousImage();
    }

    private void fireNextImage() {
        pictureActionListener.nextImage();
    }

    private void initGUI() {
        setLayout(new MigLayout("inset 0", "[][][][][]", "[]"));

        previousImageButton = new JButton("Prev");
        add(previousImageButton, "cell 0 0");
        previousImageButton.setEnabled(false);

        nextImageButton = new JButton("Next");
        add(nextImageButton, "cell 4 0");
        nextImageButton.setEnabled(false);

        drawByHandButton = new JButton("Mouse draw");
        add(drawByHandButton, "cell 5 0");

        loadButton = new JButton("Load");
        add(loadButton, "cell 1 0");

        loadSeriesButton = new JButton("Load series");
        add(loadSeriesButton, "cell 2 0");

        editButton = new JButton("Edit");
        add(editButton, "cell 3 0");
    }

    private void fireLoadAction() {
        pictureActionListener.load();
    }

    private void fireEditMode() {
        editPanelButtonsInterface.editMode();
        editButton.setText("Edit");
        editMode = true;
    }

    private void fireRubberMode() {
        editPanelButtonsInterface.rubberMode();
        editButton.setText("Rubber");
        editMode = false;
    }

    private void fireHandMode() {
        editPanelButtonsInterface.handMode();
        drawByHandButton.setText("Hand draw");
        mouseMode = true;
    }

    private void fireMouseMode() {
        editPanelButtonsInterface.mouseMode();
        drawByHandButton.setText("Mouse draw");
        mouseMode = false;
    }

    private void fireLoadSeriesAction() {
        pictureActionListener.loadSeries();
    }

    void setEditButtonEnabled(boolean enabled) {
        editButton.setEnabled(enabled);
    }

    void setNextImageButtonEnabled(boolean enabled){
        nextImageButton.setEnabled(enabled);
    }

    void setPreviousImageButtonEnabled(boolean enabled){
        previousImageButton.setEnabled(enabled);
    }
}
