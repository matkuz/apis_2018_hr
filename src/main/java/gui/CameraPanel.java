package gui;

import abstracts.AbstractCameraPanel;
import helper.ApisImageHelper;
import interfaces.ImageStreamSource;
import org.opencv.core.Mat;

import java.awt.image.BufferedImage;


public class CameraPanel extends AbstractCameraPanel {
    CameraPanel(ImageStreamSource imageStreamSource) {
        super(imageStreamSource);
    }

    @Override
    public BufferedImage processImage(Mat matFromCamera) {
        return ApisImageHelper.convertMat2BufferedImage(matFromCamera);
    }
}
