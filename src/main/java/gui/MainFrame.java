package gui;

import controllers.CameraController;

import javax.swing.*;

public class MainFrame extends JFrame {
    private MainPanel contentPanel;
    private final CameraController cameraController;

    public MainFrame() {
        super();
        this.cameraController = new CameraController();
        initGUI();
        initBehaviour();
    }

    private void initBehaviour() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void initGUI() {
        setResizable(false);
        setTitle("APIS2018");
        contentPanel = new MainPanel(cameraController);
        setContentPane(contentPanel);

        validate();
        pack();
        setVisible(true);
    }

}
