package gui;

import controllers.CameraController;
import controllers.PictureActionController;
import helper.ApisImageHelper;
import listeners.PictureActionListener;
import net.miginfocom.swing.MigLayout;
import video.VideoCap;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.util.List;

public class MainPanel extends JPanel implements PictureActionListener {
    private final PictureActionController pictureActionController;
    private final CameraController cameraController;
    private final VideoCap videoCap;
    private CameraPanel cameraPanel;
    private CameraButtonsPanel cameraButtonsPanel;
    private EditionPanel editionPanel;
    private EditButtonsPanel editButtonsPanel;
    private PalmPanel palmPanel;
    private PalmContourPanel palmContourPanel;
    private HandMovementTrackingPanel handMovementTrackingPanel;
    private MorphViewPanel morphViewPanel;

    MainPanel(CameraController cameraController) {
        super();
        this.videoCap = new VideoCap();
        this.cameraController = cameraController;
        this.pictureActionController = new PictureActionController(videoCap);

        initGUI();
        registerCameraStreamThreads();
    }

    private void registerCameraStreamThreads() {
        cameraController.registerCameraStateChangeListener(cameraPanel);
        cameraController.registerCameraStateChangeListener(handMovementTrackingPanel);
        cameraController.registerCameraStateChangeListener(morphViewPanel);
    }

    private void initGUI() {
        setLayout(new MigLayout("inset 0", "[grow][][grow][]", "[grow][][grow]"));
        cameraPanel = new CameraPanel(videoCap);
        cameraButtonsPanel = new CameraButtonsPanel(cameraController, this);

        add(cameraPanel, "cell 0 0");
        add(cameraButtonsPanel, "cell 0 1, center");

        editionPanel = new EditionPanel();
        editButtonsPanel = new EditButtonsPanel(this, editionPanel);

        add(editionPanel, "cell 2 0");
        add(editButtonsPanel, "cell 2 1, center");

        morphViewPanel = new MorphViewPanel(videoCap);
        add(morphViewPanel, "cell 0 2, center");

//        palmPanel = new PalmPanel(morphViewPanel);
//        add(palmPanel, "cell 0 2");
//
//        palmContourPanel = new PalmContourPanel(morphViewPanel);
//        add(palmContourPanel, "cell 2 2");

        handMovementTrackingPanel = new HandMovementTrackingPanel(videoCap, morphViewPanel, editionPanel);
        add(handMovementTrackingPanel, "cell 3 0");
    }


    @Override
    public void saveOne() {
        pictureActionController.saveOneImage(ApisImageHelper.convertMat2BufferedImage(videoCap.getImageSource()));
    }

    @Override
    public void startSavingSeries() {
        pictureActionController.startSavingSeries();
    }

    @Override
    public void stopSavingSeries() {
        pictureActionController.endSavingSeries();
    }

    @Override
    public void nextImage() {
        if (editionPanel.isNextImageAvailable()) {
            editionPanel.setNextImage();
        }
        checkNextPrevImageAvailability();
    }

    @Override
    public void previousImage() {
        if (editionPanel.isPrevImageAvailable()) {
            editionPanel.setPrevImage();
        }
        checkNextPrevImageAvailability();
    }


    @Override
    public void load() {
        BufferedImage imageFromFile = pictureActionController.loadImageFromFile();
        editionPanel.setActiveEditImage(imageFromFile);
        editButtonsPanel.setEditButtonEnabled(true);
    }

    @Override
    public void loadSeries() {
        List<BufferedImage> series = pictureActionController.loadSeries();
        editionPanel.setSeries(series);
        if (series.size() > 0) {
            editionPanel.setActiveEditImage(series.get(0));
            editButtonsPanel.setEditButtonEnabled(true);
            checkNextPrevImageAvailability();
        }
    }

    private void checkNextPrevImageAvailability() {
        editButtonsPanel.setPreviousImageButtonEnabled(editionPanel.isPrevImageAvailable());
        editButtonsPanel.setNextImageButtonEnabled(editionPanel.isNextImageAvailable());
    }
}
