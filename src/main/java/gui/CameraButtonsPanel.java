package gui;

import controllers.CameraController;
import listeners.PictureActionListener;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;

class CameraButtonsPanel extends JPanel {
    private JButton cameraButton;
    private JButton saveButton;
    private JButton saveSeriesButton;
    private final CameraController cameraController;
    private final PictureActionListener pictureActionListener;
    private boolean cameraActive;
    private boolean savingSeriesActive;

    CameraButtonsPanel(CameraController cameraController, PictureActionListener pictureActionListener) {
        super();
        initGUI();
        initBehaviour();
        this.pictureActionListener = pictureActionListener;
        this.cameraController = cameraController;
    }

    private void initBehaviour() {
        cameraButton.addActionListener(e -> {
            SwingUtilities.invokeLater(() -> {
                if (cameraActive) {
                    fireCameraResumed();
                } else {
                    fireCameraSuspended();
                }
            });

        });
        saveButton.addActionListener(e -> fireSaveOne());

        saveSeriesButton.addActionListener(e -> {
            SwingUtilities.invokeLater(() -> {
                if (savingSeriesActive) {
                    fireStopSavingSeries();
                } else {
                    fireStartSaving();
                }
            });
        });
    }

    private void initGUI() {
        setLayout(new MigLayout("inset 0", "[][][]", "[][]"));

        cameraButton = new JButton("Camera Off");
        add(cameraButton, "cell 1 0, center");

        saveButton = new JButton("Save");
        add(saveButton, "cell 1 1");

        saveSeriesButton = new JButton("Start saving");
        add(saveSeriesButton, "cell 1 1");
    }

    private void fireCameraResumed() {
        cameraButton.setText("Camera OFF");
        cameraController.resumeCamera();
        cameraActive = false;
    }

    private void fireCameraSuspended() {
        cameraButton.setText("Camera ON");
        cameraController.suspendCamera();
        cameraActive = true;
    }

    private void fireSaveOne() {
        pictureActionListener.saveOne();
    }

    private void fireStartSaving() {
        pictureActionListener.startSavingSeries();
        saveSeriesButton.setText("Stop Saving");
        savingSeriesActive = true;

    }

    private void fireStopSavingSeries() {
        pictureActionListener.stopSavingSeries();
        saveSeriesButton.setText("Start saving");
        savingSeriesActive = false;
    }
}
