package gui;

import interfaces.EditPanelButtonsInterface;
import listeners.EditStateChangeListener;
import listeners.DrawTechniqueListener;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class EditionPanel extends JPanel implements EditPanelButtonsInterface {
    private BufferedImage activeEitImage;
    private List<BufferedImage> series;
    private List<Point> points;
    private boolean editMode = true;
    private boolean handMode = false;
    private int widthFrame = 400;
    private int heightFrame = 400;
    EditionPanel() {
        super();
        points = new ArrayList<Point>();
        initGUI();
    }

    private void initGUI() {
        setLayout(new MigLayout("inset 0", "[grow]", "[grow]"));
        setPreferredSize(new Dimension(widthFrame, heightFrame));
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                Point point = new Point(e.getX(),e.getY());
                drawPoint(point);
            }
        });
    }

    public void drawPointFromHand(int x, int y) {
        if(handMode && editMode) {
            Point point = new Point(x, y);
            drawPoint(point);
        }
    }

    private void drawPoint(Point point){
        if(editMode) {
            points.add(point);
        } else {
            List<Point> listOfNeighbors = getNeighborsOfPoint(point.getX(), point.getY());
            System.out.println(listOfNeighbors);
            for(Point neighbor : listOfNeighbors) {
                points.remove(neighbor);
            }
            points.remove(point);
        }
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(getActiveEitImage(), 0, 0, this);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(3));
        g.setColor(Color.BLUE);
        for(Point point : points) {
            g2.draw(new Line2D.Float(point.x, point.y, point.x, point.y));
        }
    }

    BufferedImage getActiveEitImage() {
        return activeEitImage;
    }

    void setActiveEditImage(BufferedImage activeEitImage) {
        this.activeEitImage = activeEitImage;
        repaint();
    }

    void setSeries(List<BufferedImage> series) {
        this.series = series;
    }

    boolean isNextImageAvailable() {
        return activeEitImage != null && series.indexOf(activeEitImage) != series.size() - 1;
    }


    public void setNextImage() {
        setActiveEditImage(series.get(series.indexOf(activeEitImage) + 1));
    }

    public boolean isPrevImageAvailable() {
        return activeEitImage != null && series.indexOf(activeEitImage) != 0;
    }

    public void setPrevImage() {
        setActiveEditImage(series.get(series.indexOf(activeEitImage) - 1));
    }

    @Override
    public void editMode() {
        this.editMode = true;
    }

    @Override
    public void rubberMode() {
        this.editMode = false;
    }

    private List<Point> getNeighborsOfPoint(int x, int y) {
        List<Point> neighbors = new ArrayList<Point>();
        for (int xx = -1; xx <= 1; xx++) {
            for (int yy = -1; yy <= 1; yy++) {
                if (xx == 0 && yy == 0) {
                    continue;
                }
                if (Math.abs(xx) + Math.abs(yy) > 1) {
                    continue;
                }
                if (isOnMap(x + xx, y + yy)) {
                    neighbors.add(new Point(x + xx, y + yy));
                }
            }
        }
        return neighbors;
    }

    public boolean isOnMap(int x, int y) {
        return x >= 0 && y >= 0 && x < widthFrame && y < heightFrame;
    }

    @Override
    public void handMode() {
        this.handMode = true;
    }

    @Override
    public void mouseMode(){
        this.handMode = false;
    }
}
