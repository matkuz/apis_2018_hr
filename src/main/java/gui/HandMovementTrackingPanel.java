package gui;

import abstracts.AbstractCameraPanel;
import helper.ApisImageHelper;
import interfaces.ImageStreamSource;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import java.util.List;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Optional;
import org.opencv.core.Core;

import static org.opencv.imgproc.Imgproc.circle;
import static org.opencv.imgproc.Imgproc.line;
import static org.opencv.imgproc.Imgproc.putText;

public class HandMovementTrackingPanel extends AbstractCameraPanel {
    private EditionPanel editionPanel;
    private final ImageStreamSource cameraSourceImage;
    private List<Point> bufferFingers=new LinkedList<Point>();
    private Point finger=new Point();
    private static String gestureSign ="Gesture recognition";

    HandMovementTrackingPanel(ImageStreamSource cameraSourceImage,  ImageStreamSource imageStreamSource, EditionPanel editionPanel) {
        super(imageStreamSource);
        this.editionPanel = editionPanel;
        this.cameraSourceImage = cameraSourceImage;

    }

    @Override
    public BufferedImage processImage(Mat sourceImage){
        //pusty czarny obraz
        Mat resPalmMat = sourceImage.clone();

        //malowanie konturu i otaczajacego go prostokata
        Optional<MatOfPoint> biggestContour = ApisImageHelper.getBiggestContour(sourceImage);
        Mat cameraSourceImage = this.cameraSourceImage.getImageSource();
        if (biggestContour.isPresent()) {
            Rect rect = Imgproc.boundingRect(biggestContour.get());
            double centerX = rect.br().x - rect.width / 2;
            double centerY = rect.tl().y + rect.height / 2;
            // todo - make it more smarter,
            editionPanel.drawPointFromHand((int)centerX,(int)centerY);;
            Imgproc.rectangle(cameraSourceImage, rect.br(), rect.tl(), new Scalar(0, 0, 255), 1);

            circle(cameraSourceImage, new Point(centerX, centerY), 5, new Scalar(0, 255, 0), 4);

            // add fingers recognitions
            List<Point> fingerPoints = fingers(cameraSourceImage,biggestContour.get().toList(), new Point(centerX, centerY));
            if(fingerPoints.size()==1 && bufferFingers.size()<5){
                bufferFingers.add(fingerPoints.get(0));
                finger=fingerPoints.get(0);
            } else
            {
                if(fingerPoints.size()==1){
                    finger=averageFilter(bufferFingers, fingerPoints.get(0));
                }
            }
            getMiddleFingers(cameraSourceImage, new Point(centerX, centerY), finger, fingerPoints);
            // run main method
            long temp=0;
            regognizeGesture(fingerPoints,finger,new Point(centerX, centerY), true,cameraSourceImage);

        }




        return ApisImageHelper.convertMat2BufferedImage(cameraSourceImage);
    }


    public Point averageFilter(List<Point> buffer, Point current){
        Point avarage=new Point();
        avarage.x=0;
        avarage.y=0;
        for(int i=buffer.size()-1;i>0;i--){
            buffer.set(i, buffer.get(i-1));
            avarage.x=avarage.x+buffer.get(i).x;
            avarage.y=avarage.y+buffer.get(i).y;
        }
        buffer.set(0, current);
        avarage.x=(avarage.x+buffer.get(0).x)/buffer.size();
        avarage.y=(avarage.y+buffer.get(0).y)/buffer.size();
        return avarage;
    }

    public List<Point> fingers(Mat imagesource,List<Point> conturPointsList, Point centerPoint){
        List<Point> fingersPoints=new LinkedList<Point>();
        List<Point> fingers=new LinkedList<Point>();
        int range_value=55;
        for(int j=0;j<conturPointsList.size();j++){
            Point prev_point=new Point();
            Point peak_point=new Point();
            Point next_point=new Point();
            peak_point=conturPointsList.get(j);
            if(j-range_value>0){

                prev_point=conturPointsList.get(j-range_value);
            }
            else{
                int a=range_value-j;
                prev_point=conturPointsList.get(conturPointsList.size()-a-1);
            }
            if(j+range_value<conturPointsList.size()){
                next_point=conturPointsList.get(j+range_value);
            }
            else{
                int a=j+range_value-conturPointsList.size();
                next_point=conturPointsList.get(a);
            }

            Point v1= new Point();
            Point v2= new Point();
            v1.x=peak_point.x-next_point.x;
            v1.y=peak_point.y-next_point.y;
            v2.x=peak_point.x-prev_point.x;
            v2.y=peak_point.y-prev_point.y;
            double dotproduct = (v1.x*v2.x) + (v1.y*v2.y);
            double length1 = Math.sqrt((v1.x*v1.x)+(v1.y*v1.y));
            double length2 = Math.sqrt((v2.x*v2.x)+(v2.y*v2.y));
            double angle = Math.acos(dotproduct/(length1*length2));
            angle=angle*180/Math.PI;
            if(angle<60)
            {
                double centerPrev=Math.sqrt(((prev_point.x-centerPoint.x)*(prev_point.x-centerPoint.x))+((prev_point.y-centerPoint.y)*(prev_point.y-centerPoint.y)));
                double centerPeak=Math.sqrt(((peak_point.x-centerPoint.x)*(peak_point.x-centerPoint.x))+((peak_point.y-centerPoint.y)*(peak_point.y-centerPoint.y)));
                double centerNext=Math.sqrt(((next_point.x-centerPoint.x)*(next_point.x-centerPoint.x))+((next_point.y-centerPoint.y)*(next_point.y-centerPoint.y)));
                if(centerPrev<centerPeak && centerNext<centerPeak){

                    fingersPoints.add(peak_point);
                    circle(imagesource, peak_point, 2, new Scalar(200,0,230));

                    line(imagesource, peak_point, centerPoint, new Scalar(0,255,255));
                }
            }
        }

        Point averagePoint=new Point();
        averagePoint.x=0;
        averagePoint.y=0;
        int med=0;
        boolean t=false;
        if(fingersPoints.size()>0){
            double dif=Math.sqrt(((fingersPoints.get(0).x-fingersPoints.get(fingersPoints.size()-1).x)*(fingersPoints.get(0).x-fingersPoints.get(fingersPoints.size()-1).x))+((fingersPoints.get(0).y-fingersPoints.get(fingersPoints.size()-1).y)*(fingersPoints.get(0).y-fingersPoints.get(fingersPoints.size()-1).y)));
            if(dif<=20){
                t=true;
            }
        }
        for(int i=0;i<fingersPoints.size()-1;i++){

            double d=Math.sqrt(((fingersPoints.get(i).x-fingersPoints.get(i+1).x)*(fingersPoints.get(i).x-fingersPoints.get(i+1).x))+((fingersPoints.get(i).y-fingersPoints.get(i+1).y)*(fingersPoints.get(i).y-fingersPoints.get(i+1).y)));

            if(d>20 || i+1==fingersPoints.size()-1){
                Point p=new Point();

                p.x=(int)(averagePoint.x/med);
                p.y=(int)(averagePoint.y/med);

                fingers.add(p);

                if(t && i+1==fingersPoints.size()-1){
                    Point ult=new Point();
                    if(fingers.size()>1){
                        ult.x=(fingers.get(0).x+fingers.get(fingers.size()-1).x)/2;
                        ult.y=(fingers.get(0).y+fingers.get(fingers.size()-1).y)/2;
                        fingers.set(0, ult);
                        fingers.remove(fingers.size()-1);
                    }
                }
                med=0;
                averagePoint.x=0;
                averagePoint.y=0;
            }
            else{

                averagePoint.x=(averagePoint.x+fingersPoints.get(i).x);
                averagePoint.y=(averagePoint.y+fingersPoints.get(i).y);
                med++;


            }
        }


        return fingers;
    }



    public void getMiddleFingers(Mat image, Point centerPoint, Point finger, List<Point> fingers){
        if(fingers.size()==1){
            line(image, centerPoint, finger, new Scalar(0, 255, 255),4);
            circle(image, finger, 3, new Scalar(255,0,255),3);
        }
        else
        {
            for(int i=0;i<fingers.size();i++){
                line(image, centerPoint, fingers.get(i), new Scalar(0, 255, 255),4);
                circle(image, fingers.get(i), 3, new Scalar(255,0,255),3);
            }
        }
        circle(image, centerPoint, 3, new Scalar(0,0,255),3);
    }


    public void regognizeGesture(List<Point> fingers,Point finger,Point centerPoint,boolean on,Mat sourceImg){

        if(on && centerPoint.x>10 && centerPoint.y>10 && finger.x>10 && centerPoint.y>10){
            switch(fingers.size()){
                case 0:
                    gestureSign ="Gesture 1";
                    break;
                case 1:
                    gestureSign ="Gesture 2";
                    break;
                case 2:
                    gestureSign ="Gesture 3";
                    break;
                case 3:
                    gestureSign ="Gesture 4";
                    break;
                case 4:
                    gestureSign ="Gesture 5";
                    break;

                case 5:
                    gestureSign ="Gesture 5";
                    break;
                default:
                    gestureSign ="Waiting...";
                    break;
            }

        }
        else{
        }
        putText(sourceImg, gestureSign,new Point(50,40), Core.FONT_HERSHEY_COMPLEX, 1, new Scalar(200,0,0));

    }



}

