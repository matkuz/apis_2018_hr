package gui;

import abstracts.AbstractCameraPanel;
import helper.ApisImageHelper;
import interfaces.ImageStreamSource;
import org.opencv.core.Mat;

import java.awt.image.BufferedImage;

public class MorphViewPanel extends AbstractCameraPanel implements ImageStreamSource {
    private Mat processedImage;

    public MorphViewPanel(ImageStreamSource imageStreamSource) {
        super(imageStreamSource);
    }

    @Override
    public BufferedImage processImage(Mat matFromCamera) {
        processedImage = ApisImageHelper.getMorphImage(matFromCamera);
        return ApisImageHelper.convertMat2BufferedImage(processedImage);
    }

    @Override
    public Mat getImageSource() {
        return processedImage != null ? processedImage.clone() : processedImage;
    }
}
