package gui;

import abstracts.AbstractCameraPanel;
import helper.ApisImageHelper;
import interfaces.ImageStreamSource;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class PalmContourPanel extends AbstractCameraPanel {
    PalmContourPanel(ImageStreamSource imageStreamSource) {
        super(imageStreamSource);
    }

    @Override
    public BufferedImage processImage(Mat sourceImage) {
        //pusty czarny obraz
        Mat resPalmMat = ApisImageHelper.getCleanBlackImage(sourceImage);

        //malowanie konturu i otaczajacego go prostokata
        Optional<MatOfPoint> biggestContour = ApisImageHelper.getBiggestContour(sourceImage);
        if (biggestContour.isPresent()) {
            List<MatOfPoint> contours = new LinkedList<>();
            contours.add(biggestContour.get());
            Rect rect = Imgproc.boundingRect(biggestContour.get());
            double centerX = rect.br().x - rect.width / 2;
            double centerY = rect.tl().y + rect.height / 2;
            Imgproc.circle(resPalmMat, new Point(centerX, centerY), 5, new Scalar(0, 255, 0), 4);

            Imgproc.drawContours(resPalmMat, contours, contours.indexOf(biggestContour.get()), new Scalar(255, 0, 0), 1);
            Imgproc.rectangle(resPalmMat, rect.br(), rect.tl(), new Scalar(0, 0, 255), 1);
        }

        return ApisImageHelper.convertMat2BufferedImage(resPalmMat);
    }
}
