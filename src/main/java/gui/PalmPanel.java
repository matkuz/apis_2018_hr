package gui;

import abstracts.AbstractCameraPanel;
import helper.ApisImageHelper;
import interfaces.ImageStreamSource;


import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;


import java.awt.image.BufferedImage;
import java.util.*;

public class PalmPanel extends AbstractCameraPanel {

    PalmPanel(ImageStreamSource imageStreamSource) {
        super(imageStreamSource);
    }

    @Override
    public BufferedImage processImage(Mat sourceImage) {
        //pusty czarny obraz na ktorym bedzie namalowany kontur
        Mat resPalmMat = ApisImageHelper.getCleanBlackImage(sourceImage);

        //wypelnianie najwiekszego konturu
        Optional<MatOfPoint> biggestContour = ApisImageHelper.getBiggestContour(sourceImage);
        biggestContour.ifPresent(matOfPoint -> Imgproc.fillPoly(resPalmMat, Collections.singletonList(matOfPoint), new Scalar(255, 255, 255)));

        return ApisImageHelper.convertMat2BufferedImage(resPalmMat);
    }


}
