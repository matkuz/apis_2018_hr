package abstracts;

import interfaces.ImageStreamSource;
import listeners.CameraStateChangeListener;
import net.miginfocom.swing.MigLayout;
import org.opencv.core.Mat;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class AbstractCameraPanel extends JPanel implements CameraStateChangeListener {
    private final PanelUpdater panelUpdater;
    private final ImageStreamSource imageStreamSource;

    public AbstractCameraPanel(ImageStreamSource imageStreamSource) {
        super();
        initGUI();
        this.imageStreamSource = imageStreamSource;
        this.panelUpdater = createPanelUpdater();
    }

    private PanelUpdater createPanelUpdater() {
        PanelUpdater panelUpdater = new PanelUpdater() {
            @Override
            public void updateAction() {
                repaint();
            }
        };
        panelUpdater.start();

        return panelUpdater;
    }

    private void initGUI() {
        setLayout(new MigLayout("inset 0", "[grow]", "[grow]"));
        setPreferredSize(new Dimension(400, 400));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Mat imageSource = imageStreamSource.getImageSource();
        if (imageSource != null) {
            BufferedImage processedImage = processImage(imageSource);
            g.drawImage(processedImage, 0, 0, this);
        }

    }

    @Override
    public void cameraSuspended() {
        panelUpdater.suspend();
    }

    @Override
    public void cameraResumed() {
        panelUpdater.resume();
    }

    @Override
    public void cameraStopped() {
        panelUpdater.stop();
    }

    public abstract BufferedImage processImage(Mat matFromCamera);
}
