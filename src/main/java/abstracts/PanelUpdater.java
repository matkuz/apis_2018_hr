package abstracts;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class PanelUpdater {
    private UpdaterThread updaterThread;
    private final AtomicBoolean running = new AtomicBoolean(true);
    private final AtomicBoolean suspendedOrStopped = new AtomicBoolean(false);

    void start() {
        updaterThread = new UpdaterThread();
        updaterThread.start();
    }

    void stop() {
        suspend();
        running.set(false);
    }

    void suspend() {
        suspendedOrStopped.set(true);
        updaterThread.suspend();
    }

    void resume() {
        suspendedOrStopped.set(false);
        updaterThread.resume();
    }

    public abstract void updateAction();

    private class UpdaterThread extends Thread {
        @Override
        public void run() {
            while (running.get()) {
                if (!suspendedOrStopped.get()) {
                    updateAction();
                }
            }
            System.out.println(
                    Thread.currentThread().getName() + "Updater thread stopped");
        }
    }
}