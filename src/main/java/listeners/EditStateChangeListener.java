package listeners;

public interface EditStateChangeListener {
    void editMode();
    void rubberMode();
}
