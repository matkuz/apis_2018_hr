package listeners;

public interface CameraStateChangeListener {
    void cameraSuspended();

    void cameraResumed();

    void cameraStopped();
}
