package listeners;

public interface PictureActionListener {
    void saveOne();

    void startSavingSeries();

    void stopSavingSeries();

    void load();

    void nextImage();

    void previousImage();

    void loadSeries();
}
