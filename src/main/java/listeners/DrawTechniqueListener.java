package listeners;

public interface DrawTechniqueListener {
    void handMode();
    void mouseMode();
}
