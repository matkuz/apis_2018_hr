package video;


import interfaces.ImageStreamSource;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

public class VideoCap implements ImageStreamSource {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    private final VideoCapture cap;
    private final Mat2Image mat2Img = new Mat2Image();

    public VideoCap() {
        cap = new VideoCapture();
        cap.open(0);
    }

    @Override
    public Mat getImageSource() {
        cap.read(mat2Img.mat);
        return mat2Img.mat;
    }
}