import gui.MainFrame;
import javax.swing.*;


public class ApisStarter {
    static {
        nu.pattern.OpenCV.loadShared();
        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
        } catch (Exception e) {
            System.out.println("Błąd podczas ustawiania motywu");
        }
    }

    public static void main(String[] args) {
        new MainFrame();
    }
}
