package controllers;

import listeners.CameraStateChangeListener;

import java.util.LinkedList;
import java.util.List;

public class CameraController {
    private final List<CameraStateChangeListener> cameraStateChangeListeners = new LinkedList<>();

    public void registerCameraStateChangeListener(CameraStateChangeListener cameraStateChangeListener) {
        cameraStateChangeListeners.add(cameraStateChangeListener);
    }

    public void removeCameraStateChangeListener(CameraStateChangeListener cameraStateChangeListener) {
        cameraStateChangeListeners.remove(cameraStateChangeListener);
    }


    public void resumeCamera() {
        cameraStateChangeListeners.forEach(CameraStateChangeListener::cameraResumed);
        System.out.println("KAMERA WZNOWIONA");
    }

    public void suspendCamera() {
        cameraStateChangeListeners.forEach(CameraStateChangeListener::cameraSuspended);
        System.out.println("KAMERA ZATRZYMANA");
    }

    public void stopCamera() {
        cameraStateChangeListeners.forEach(CameraStateChangeListener::cameraStopped);
        System.out.println("KAMERA WYLACZONA");
    }
}
