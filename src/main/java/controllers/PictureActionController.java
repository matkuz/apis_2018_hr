package controllers;

import helper.ApisImageHelper;
import interfaces.ImageStreamSource;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class PictureActionController {
    public static final String TMP_FOLDER_PATH = "C:\\Users\\johny\\Documents\\java\\apismain\\series\\";
    private final ImageStreamSource imageStreamSource;
    private SeriesSavingThread seriesSavingThread;
    private final AtomicBoolean running = new AtomicBoolean(false);

    public PictureActionController(ImageStreamSource imageStreamSource) {
        this.imageStreamSource = imageStreamSource;
    }

    public void saveOneImage(BufferedImage bufferedImage) {
        JFileChooser jfc = new JFileChooser();
        int res = jfc.showSaveDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            File f = jfc.getSelectedFile();
            String path = f.getAbsolutePath();

            try {
                ImageIO.write(bufferedImage, "png", new File(path));
            } catch (IOException e) {
                System.out.println("Błąd podczas obrazu do pliku");
            }
        }
    }

    public void endSavingSeries() {
        running.set(false);
    }

    public void startSavingSeries() {
        running.set(true);
        seriesSavingThread = new SeriesSavingThread();
        seriesSavingThread.start();
    }


    public BufferedImage loadImageFromFile() {
        JFileChooser chooser = new JFileChooser();
        BufferedImage img = null;
        File file;
        int res = chooser.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
            try {
                img = ImageIO.read(file);
            } catch (IOException e1) {
                System.out.println("Błąd podczas zapisu obrazu do pliku");
            }
        }
        return img;
    }

    public List<BufferedImage> loadSeries() {
        List<BufferedImage> series = new LinkedList<>();
        String dirPath = TMP_FOLDER_PATH;
        File dir = new File(dirPath);
        for (File tmpFile : dir.listFiles()) {
            try {
                BufferedImage image = ImageIO.read(tmpFile);
                series.add(image);
            } catch (Exception e) {
                System.out.println("Błąd podczas ladowania obrazow");
            }
        }
        return series;
    }

    private class SeriesSavingThread extends Thread {
        @Override
        public void run() {
            String dirPath = TMP_FOLDER_PATH;
            File dir = new File(dirPath);
            for (File tmpFile : dir.listFiles()) {
                tmpFile.delete();
            }
            int i = 0;
            System.out.println("Zapis obrazow rozpoczety");
            while (running.get()) {
                try {

                    BufferedImage imageFromCamera = ApisImageHelper.convertMat2BufferedImage(imageStreamSource.getImageSource());
                    ImageIO.write(imageFromCamera, "png", new File(dirPath + i + ".png"));
                    Thread.sleep(30);
                    i++;
                } catch (IOException | NullPointerException | InterruptedException e) {
                    System.out.println("Błąd podczas zapisu obrazow");
                    running.set(false);
                }
            }
            System.out.println("Zapis obrazow zakonczony");
        }
    }

}
